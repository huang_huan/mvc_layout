﻿using System.Web;
using System.Web.Mvc;

namespace Upgrade_ManWH
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
